package models;

import constants.Constants;

import java.util.LinkedList;

class Bin {

    private SmallBin[] smallBins = new SmallBin[Constants.BIN_COUNT];
    private Chunk topChunk = new Chunk(Constants.TOTAL_MEMORY, false);
    private LinkedList<Chunk> chunks = new LinkedList<>();
    private final Mode mode;
    //small bin id
    private int next_fit_position = 0;

    Bin(Mode mode) {
        this.mode = mode;
        for (int i = 0; i < Constants.BIN_COUNT; i++) {
            smallBins[i] = new SmallBin();
        }
    }


    void malloc(int size) {
        size = roundUp(size);
        int remain = -1;
        int index = -1;
        int id = -1;
        switch (mode) {
            case FirstFit:
                for (Chunk chunk : chunks) {
                    if (!chunk.isUsed()) {
                        if (chunk.getSize() >= size) {
                            id = smallBinIdBySize(chunk.getSize());
                            break;
                        }
                    }
                }
                if (id != -1) {
                    Chunk chunk = smallBins[id].malloc();
                    if (chunk != null) {
                        remain = chunk.getSize() - size;

                        Chunk newChunk = new Chunk(size, true);
                        index = chunks.indexOf(chunk);
                        chunks.add(index, newChunk);
                        chunks.remove(chunk);
                        System.out.println("Memory is successfully allocated");
                    }
                }
                //first time use topchunck
                handelRemain(size, remain, index);
                break;
            case NextFit:
                for (int i = 0; i < smallBins.length; i++) {
                    SmallBin smallBin = smallBins[next_fit_position];
                    if (smallBin.getItemCount() > 0) {
                        if ((next_fit_position + 1) * 8 >= size) {
                            id = next_fit_position;
                            break;
                        }
                    }
                    next_fit_position++;
                    if (next_fit_position >= smallBins.length) {
                        next_fit_position = 0;
                    }
                }
                if (id != -1) {
                    Chunk chunk = smallBins[id].malloc();
                    if (chunk != null) {
                        remain = chunk.getSize() - size;

                        Chunk newChunk = new Chunk(size, true);
                        index = chunks.indexOf(chunk);
                        chunks.add(index, newChunk);
                        chunks.remove(chunk);
                        System.out.println("Memory is successfully allocated");
                    }
                }
                handelRemain(size, remain, index);
                break;
            case WorstFit:
                for (int i = Constants.BIN_COUNT - 1; i > smallBinIdBySize(size); i--) {
                    if (smallBins[i].getItemCount() > 0) {
                        id = i;
                        break;
                    }
                }
                if (id != -1) {
                    Chunk chunk = smallBins[id].malloc();
                    if (chunk != null) {
                        remain = chunk.getSize() - size;

                        Chunk newChunk = new Chunk(size, true);
                        index = chunks.indexOf(chunk);
                        chunks.add(index, newChunk);
                        chunks.remove(chunk);
                        System.out.println("Memory is successfully allocated");
                    }
                }
                handelRemain(size, remain, index);
                break;
        }
        print();
    }

    private void handelRemain(int size, int remain, int index) {
        if (remain == -1) {
            if (size > topChunk.getSize()) {
                System.out.println("out of memory");
            } else {
                Chunk newChunk = new Chunk(size, true);
                newChunk.setPrevChunkSize(topChunk.getPrevChunkSize());
                topChunk.setPrevChunkSize(size);
                topChunk.setSize(topChunk.getSize() - size);
                chunks.add(newChunk);
                System.out.println("Memory is successfully allocated");
            }
        } else {
            if (remain > 0) {
                Chunk chunk = new Chunk(remain, false);
                chunk.setPrevChunkSize(size);
                chunks.add(index + 1, chunk);
                getSmallBinBySize(remain).add(chunk, chunks);
            }
        }
    }

    void printOut() {
        for (int i = 0; i < Constants.BIN_COUNT; i++) {
            if (smallBins[i].getItemCount() > 0) {
                System.out.println("bin" + (i + 1) + " " + smallBins[i].getItemCount());
            }
        }
        print();
    }

    private void print() {
        int number = 0;
        for (Chunk chunk : chunks) {
            if (!chunk.isUsed()) {
                number += chunk.getSize();
            } else {
                System.out.print(number + " ");
                number += chunk.getSize();
            }
        }
        System.out.println(number + " ");
    }

    void free(int size) {
        size = roundUp(size);
        for (Chunk chunk : chunks) {
            if (chunk.getSize() == size) {
                if (chunk.isUsed()) {
                    Chunk freeChunk = new Chunk(size, false);
                    freeChunk.setPrevChunkSize(chunk.getPrevChunkSize());
                    getSmallBinBySize(size).add(freeChunk, chunks);
                    int index = chunks.indexOf(chunk);
                    chunks.add(index, freeChunk);
                    chunks.remove(chunk);

                    mergeFree();
                    System.out.println("Memory is successfully deallocated");
                    for (int i = 0; i < Constants.BIN_COUNT; i++) {
                        if (smallBins[i].getItemCount() > 0) {
                            System.out.println("bin" + (i + 1) + " " + smallBins[i].getItemCount());
                        }
                    }

                    return;

                }
            }
        }
        System.out.println("Invalid memory deallocation");
    }

    private void mergeFree() {
        for (int i = chunks.size() - 1; i > 0; i--) {
            Chunk temp = chunks.get(i);
            if (!temp.isUsed()) {
                Chunk other = chunks.get(i - 1);
                if (!other.isUsed()) {
                    Chunk newOne = new Chunk(temp.getSize() + other.getSize(), false);
                    newOne.setPrevChunkSize(other.getPrevChunkSize());

                    chunks.add(i, newOne);
                    chunks.remove(temp);
                    getSmallBinBySize(temp.getSize()).remove(temp);
                    chunks.remove(other);
                    getSmallBinBySize(other.getSize()).remove(other);

                    getSmallBinBySize(newOne.getSize()).add(newOne, chunks);

                    mergeFree();
                    return;
                }
            }
        }
        if (!chunks.getLast().isUsed()) {
            Chunk chunk = chunks.getLast();
            getSmallBinBySize(chunk.getSize()).remove(chunk);
            chunks.remove(chunk);
            topChunk.setSize(chunk.getSize() + topChunk.getSize());
        }
    }

    private int smallBinIdBySize(int size) {
        return (size / 8) - 1;
    }

    private SmallBin getSmallBinBySize(int size) {
        return smallBins[smallBinIdBySize(size)];
    }

    private static int roundUp(int size) {
        if (size % 8 == 0) {
            return size;
        } else {
            return size + 8 - (size % 8);
        }
    }

}
