package models.data_structure;

public class TwoWayRecursiveLinkedList<T extends Node> {

    private Node head = null;
    private int itemCount = 0;

    public int getItemCount() {
        return itemCount;
    }

    public void addBefore(T newOne, T before) {
        before.getPrev().setNext(newOne);
        newOne.setNext(before);
        newOne.setPrev(before.getPrev());
        before.setPrev(newOne);
        itemCount++;
    }

    public void add(T temp) {
        if (temp != null) {
            if (head == null) {
                head = temp;
                head.setNext(head);
                head.setPrev(head);
            } else {
                temp.setNext(head);
                head.getPrev().setNext(temp);
                temp.setPrev(head.getPrev());
                head.setPrev(temp);
            }
            itemCount++;
        }
    }

    public void remove(T temp) {
        if (itemCount == 0) {
            return;
        }
        Node target = head;
        if (itemCount == 1) {
            if (head.isEqual(temp)) {
                head = null;
                itemCount--;
            }
        } else {
            for (int i = 0; i < itemCount; i++) {
                if (target.isEqual(temp)) {
                    target.getNext().setPrev(target.getPrev());
                    target.getPrev().setNext(target.getNext());
                    itemCount--;
                    return;
                } else {
                    target = head.getNext();
                }
            }
        }
    }

    public T getHead() {
        return (T) head;
    }

}
