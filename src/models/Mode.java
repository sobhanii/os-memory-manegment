package models;

public enum Mode {

    FirstFit, NextFit, WorstFit

}
