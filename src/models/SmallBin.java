package models;

import models.data_structure.TwoWayRecursiveLinkedList;

import java.util.LinkedList;

public class SmallBin {

    private static int id = 1;

    private final int ID;
    private TwoWayRecursiveLinkedList<Chunk> linkedList = new TwoWayRecursiveLinkedList<>();

    SmallBin() {
        this.ID = ++id;
    }

    public int getID() {
        return ID;
    }

    public int getItemCount() {
        return linkedList.getItemCount();
    }

    public void remove(Chunk target) {
        linkedList.remove(target);
    }

    public void add(Chunk chunk, LinkedList<Chunk> chunks) {
        if (getItemCount() == 0) {
            linkedList.add(chunk);
        } else {
            Chunk head = linkedList.getHead();
            int newIndex = chunks.indexOf(chunk);
            for (int i = 0; i < getItemCount(); i++) {
                int index = chunks.indexOf(head);
                if (newIndex < index) {
                    linkedList.addBefore(chunk, head);
                    return;
                }
                head = (Chunk) head.getNext();
            }
            linkedList.add(chunk);
        }
    }

    //try to malloc with input size
    public Chunk malloc() {
        if (getItemCount() == 0) {
            return null;
        } else {
            Chunk chunk = linkedList.getHead();
            linkedList.remove(chunk);
            return chunk;
        }
    }


}
