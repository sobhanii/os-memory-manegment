package models;

import models.data_structure.Node;

public class Chunk extends Node {

    private int size;
    private int prevChunkSize;
    private boolean used;

    public boolean isUsed() {
        return used;
    }

    public void setUsed(boolean used) {
        this.used = used;
    }

    public Chunk(int size, boolean used) {
        this.size = size;
        this.used = used;
        prevChunkSize = -1;
    }

    public int getPrevChunkSize() {
        return prevChunkSize;
    }

    public void setPrevChunkSize(int prevChunkSize) {
        this.prevChunkSize = prevChunkSize;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    @Override
    public boolean isEqual(Node node) {
        if (node != null) {
            if (node instanceof Chunk) {
                Chunk chunk = (Chunk) node;
                return this.size == chunk.size &&
                        this.prevChunkSize == chunk.prevChunkSize;
            }
        }
        return false;
    }
}
