package models;

import constants.Action;

import java.util.ArrayList;

public class Memory {

    private ArrayList<Action> actions;

    public Memory(ArrayList<Action> actions) {
        this.actions = actions;
    }

    public void run() {
        Bin bin = new Bin(Mode.FirstFit);
        for (Action action : actions) {
            if (action.isMalloc()) {
                bin.malloc(action.getSize());
            } else {
                bin.free(action.getSize());
            }
        }
        System.out.println();
        bin.printOut();
        System.out.println();

        bin = new Bin(Mode.NextFit);
        for (Action action : actions) {
            if (action.isMalloc()) {
                bin.malloc(action.getSize());
            } else {
                bin.free(action.getSize());
            }
        }
        System.out.println();
        bin.printOut();
        System.out.println();

        bin = new Bin(Mode.WorstFit);
        for (Action action : actions) {
            if (action.isMalloc()) {
                bin.malloc(action.getSize());
            } else {
                bin.free(action.getSize());
            }
        }
        System.out.println();
        bin.printOut();

    }

}
