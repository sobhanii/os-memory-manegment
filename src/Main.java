import constants.Action;

import java.util.ArrayList;
import java.util.Scanner;

import models.Memory;

public class Main {

    public static void main(String[] args) {

        ArrayList<Action> actions = new ArrayList<>();

        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNextLine()) {
            String nextLine = scanner.nextLine();
            if (nextLine.equals("end"))
                break;
            String[] temp = nextLine.split(" ");
            if (temp[0].equals("malloc") || temp[0].equals("free")) {
                boolean malloc = temp[0].equals("malloc");
                int size = Integer.parseInt(temp[1]);
                actions.add(new Action(malloc, size));
            }
        }

        Memory memory = new Memory(actions);
        memory.run();


    }

}
