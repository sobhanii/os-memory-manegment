package constants;

public class Action {

    private boolean malloc;
    private int size;

    public Action(boolean malloc, int size) {
        this.malloc = malloc;
        this.size = size;
    }

    public boolean isMalloc() {
        return malloc;
    }

    public void setMalloc(boolean malloc) {
        this.malloc = malloc;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    @Override
    public String toString() {
        return "Action{" +
                "malloc=" + malloc +
                ", size=" + size +
                '}';
    }
}
